# README #

This repository include 3 APIs:

* To create an order: POST /orders

* To list orders : GET /orders?limit=1&page=1

Limit and page can be not set, once page set, limit must set

Both limit and page parameter need to be integer greater than 1

* To take an order : PATCH /orders/:id

To call the api in localhost:

    POST localhost:8080/orders
    GET localhost:8080/orders
    PATCH localhost:8080/orders/:id

This repository will be auto tested with Bitbucket pipeline.

This repository is 100% test covered.

## Set up ##

### Summary of set up###
* Set up configuration file(detail is in below)

* Without Docker (need to have NodeJS and MongonDB install):

In the root of the repository, run "npm install"

With database(MongoDB) up, run "npm run start"

When in development, can run "npm run dev"

* With Docker: (run production mode)

In the root of the repository, run "sh start.sh"

### Configuration ###
* In production, configuration file located in /config/local.env
* In development mode, configuration file located in /config/dev.env
* Need to write in Google Map API in config file with key: GOOGLE_API_KEY

### Dependencies ###
* Docker, docker composer 
* Without Docker: NodeJS, MongoDB

### Database configuration ###
* When running in Docker, database listen to port 27017

### How to run tests ###
* Without Docker, with npm,NodeJs,MongoDB installed, run "npm test"

Test run with configuration file: /config/test.env


* With Docker, change CMD in ./Dockerfile (detail inside the file)

Test run with configuration file: /config/test.env.docker, please rename it to test.env
    

