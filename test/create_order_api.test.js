/**
 * This file contains test for creat order API
 * POST /orders
 */
const {request,distance,app,Order,HTTP,setup,down} = require('./setting')

beforeAll(async()=>{
    await setup()
})

test('Create Order Fail : Missing Destination', async () => {
    const res = await request(app).post('/orders').send({
        origin: ['40.66',"-73.89"]
    }).expect(HTTP.BAD_REQUEST)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/Missing Data/)
    })
})

test('Create Order Fail : Pass in data that is not allowed', async () => {
    const res = await request(app).post('/orders').send({
        origin: ['40.66',"-73.89"],
        destination: ['40.66',"-73.89"],
        method: "car"
    }).expect(HTTP.BAD_REQUEST)
    expect(res.body).toMatchObject({
        error: expect.stringMatching(/not allowed/)
    })

})

test('Create Order Fail : Missing Origin', async () => {
    const res = await request(app).post('/orders').send({
        destination: ['40.66',"-73.89"]
    }).expect(HTTP.BAD_REQUEST)
    expect(res.body).toMatchObject({
        error:expect.stringMatching(/Missing Data/)
    })

})

test('Create Order Fail : Coordinates is not array', async () => {
    const res = await request(app).post('/orders').send({
        origin: '40.66,-73.89',
        destination: ['40.66','-73.89']
    }).expect(HTTP.BAD_REQUEST)
    expect(res.body).toMatchObject({
        error: expect.stringMatching(/must be an array/),
    })

})

test('Create Order Fail : Coordinates array does not contain 2 elements', async () => {
    const res = await request(app).post('/orders').send({
        origin: ['40.66'],
        destination: ['40.66','-73.89']
    }).expect(HTTP.BAD_REQUEST)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/2 elements/),
    })

})

test('Create Order Fail : Coordinates element is not string', async () => {
    const res = await request(app).post('/orders').send({
        origin: ['40.66',-73.89],
        destination: ['40.66','-73.89']
    }).expect(HTTP.BAD_REQUEST)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/must be string/),
    })

})

test('Create Order Fail : Coordinates elements are not valid', async () => {
    const res = await request(app).post('/orders').send({
        origin: ['40.66','-730.89'],
        destination: ['40.66','-73.89']
    }).expect(HTTP.BAD_REQUEST)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/Coordinates are not valid/),
    })
})

test('Create Order Success', async () => {
    const res = await request(app).post('/orders').send({
        origin: ['40.66','-73.89'],
        destination: ['40.69','-73.99']
    }).expect(HTTP.CREATED)

    expect(res.body).toMatchObject({
        id: expect.any(String),
        distance,
        status: "UNASSIGNED"
    })
    
    const order = Order.findById(res.body.id);
    expect(order).not.toBeNull();
})


test('Create Order Fail : Server error', async () => {
    const OrderSaveMock = jest.spyOn(Order.prototype, 'save');
    const OrderSave = jest.fn(() => {
        throw new Error("Server error");
    });
    OrderSaveMock.mockImplementation(OrderSave);

    const res = await request(app).post('/orders').send({
        origin: ['40.66','-73.89'],
        destination: ['40.66','-73.89']
    }).expect(HTTP.SERVER_ERROR)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/Server error in saving order/),
    })
    OrderSaveMock.mockRestore();
})


afterAll(async ()=>{
    await down()
})
