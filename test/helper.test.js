/**
 * This file contains tests for /src/utils/helper.js
 */
const {
    coordinatesValidate,
    intValidate,
    getSkip,
    getErrorReturn} = require('../src/utils/helper')

    test('Validate coordinates: Invalid when value is not array',()=>{
        const result = coordinatesValidate("23,25");
        expect(result.validate).toBe(false)
        expect(result.errorMsg).toMatch("Coordinates must be an array")
        
    })

    test('Validate coordinates: Invalid when length of array is not 2',()=>{
        const result = coordinatesValidate(["1"]);
        expect(result.validate).toBe(false)
        expect(result.errorMsg).toMatch("Coordinates array must be 2 elements.")
        
    })

    test('Validate coordinates: Invalid when element in array is not string',()=>{
        const result = coordinatesValidate([23,25.456]);
        expect(result.validate).toBe(false)
        expect(result.errorMsg).toMatch("Coordinates element must be string.")
        
    })

    test('Validate coordinates: Invalid when coordinates are not valid.',()=>{
        const result = coordinatesValidate(["23","250"]);
        expect(result.validate).toBe(false)
        expect(result.errorMsg).toMatch("Coordinates are not valid.")
        
    })

    test('Validate coordinates: Valid',()=>{
        const result = coordinatesValidate(["23","25.456"]);
        expect(result.validate).toBe(true)
        
    })

    test('Validate int: Invalid when value is not int',()=>{
        const result = intValidate("a");
        expect(result).toBe(false)
        
    })

    test('Validate int: Valid when value is int and meet the condition min 1',()=>{
        const result = intValidate("1");
        expect(result).toBe(true)
        
    })

    test('Validate int: Invalid when value does not meet the condition min 1',()=>{
        const result = intValidate("0");
        expect(result).toBe(false)
        
    })

    test('Get skip number base on page number and limit', () => {
        const skip = getSkip(10,2)
        expect(skip).toBe(10)
    })


    test("Get error message object",()=>{
        const errorMsg = "Server error"
        const result = getErrorReturn(errorMsg)
        expect(result).toMatchObject({
            error:errorMsg
        })
    })