/**
 * This file contains test for get orders API
 * GET /orders
 */

const {request,distance,app,mongoose,Order,HTTP,setup,down} = require('./setting')


var orderIDs = [];
while(orderIDs.length<10){
    orderIDs.push(new mongoose.Types.ObjectId())
} 
var orderData = {
    origin: ["40.66","-73.89"],
    destination :["40.69","-73.99"]
}

const resObject = {
    id: expect.any(String),
    distance,
    status: "UNASSIGNED"
}

beforeAll(async ()=>{
    setup();
    for(id of orderIDs){
        orderData._id=id;
        await new Order(orderData).save();
    }
})

test('Get Orders Success : Without page and limit', async () => {
    const res = await request(app).get('/orders').send().expect(HTTP.SUCCESS)
    const orders = res.body;
    expect(orders.length).toBe(orderIDs.length);
    expect(orders[0]).toMatchObject(resObject)

})

test('Get Orders Success : With limit', async () => {
    let limit = 3;
    const res = await request(app).get('/orders?limit='+limit).send().expect(HTTP.SUCCESS)
    const orders = res.body;
    expect(orders.length).toBe(limit);
    expect(orders[0]).toMatchObject(resObject)

})

test('Get Orders Success : With limit and Page 1', async () => {
    let limit = 3;
    let page = 1;
    let url = '/orders?limit='+limit+'&page='+page;
    const res = await request(app).get(url).send().expect(HTTP.SUCCESS)
    const orders = res.body;

    expect(orders.length).toBe(limit);
    
    expect(orders[0]).toMatchObject(resObject)

    expect(orders[0].id).toEqual(orderIDs[0].toString());

})

test('Get Orders Success : With limit and Page 2', async () => {
    let limit = 3;
    let page = 2;
    let url = '/orders?limit='+limit+'&page='+page;
    const res = await request(app).get(url).send().expect(HTTP.SUCCESS)
    const orders = res.body;

    expect(orders.length).toBe(limit);
    
    expect(orders[0]).toMatchObject(resObject)

    expect(orders[0].id).toEqual(orderIDs[3].toString());

})


test('Get Orders Fail : limit is not integer', async () => {
    let limit = 3.2;
    let page = 1;
    let url = '/orders?limit='+limit+'&page='+page;
    const res = await request(app).get(url).send().expect(HTTP.BAD_REQUEST)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/not an integer/),
    })
})

test('Get Orders Fail : Page is not integer', async () => {
    let limit = 3;
    let page = 'a';
    let url = '/orders?limit='+limit+'&page='+page;
    const res = await request(app).get(url).send().expect(HTTP.BAD_REQUEST)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/not an integer/),
    })

})

test('Get Orders Fail : Page is less than 1', async () => {
    let limit = 3;
    let page = 0;
    let url = '/orders?limit='+limit+'&page='+page;
    const res = await request(app).get(url).send().expect(HTTP.BAD_REQUEST)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/>= 1/),
    })

})

test('Get Orders Fail : Limit is less than 1', async () => {
    let limit = 0;
    let page = 1;
    let url = '/orders?limit='+limit+'&page='+page;
    const res = await request(app).get(url).send().expect(HTTP.BAD_REQUEST)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/>= 1/),
    })

})

test('Get Orders Fail : Page is not passed in with limit', async () => {
    let page = 1;
    let url = '/orders?page='+page;
    const res = await request(app).get(url).send().expect(HTTP.BAD_REQUEST)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/with limit/),
    })

})

test('Create Order Fail : Server error', async () => {
    const OrderFindMock = jest.spyOn(Order, 'find');
    const OrderFind = jest.fn(() => {
        throw new Error("Server error");
    });
    OrderFindMock.mockImplementation(OrderFind);

    const res = await request(app).get('/orders').send().expect(HTTP.SERVER_ERROR)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/Server error in getting order/),
    })
    OrderFindMock.mockRestore();
})

// This test must be the last, because it delete all the test orders
test('Get Orders Success : No order exist, return []', async () => {
    await Order.deleteMany();
    let limit = 3;
    let page = 1;
    let url = '/orders?limit='+limit+'&page='+page;
    const res = await request(app).get(url).send().expect(HTTP.SUCCESS)
    const orders = res.body;

    expect(orders.length).toBe(0);
    
    expect(orders).toMatchObject([])


})
afterAll(async ()=>{
    await down();
})