/**
 * This file contains set up data and methods for Unit testing
 */

const request = require('supertest')
const app = require('../src/app')
const Order = require('../src/model/order')
const HTTP = require('../src/utils/http_code')
const mongoose = require('mongoose');
const axios = require('axios')
jest.mock('axios')
// Mock google api response 
const distance = 200
const googleRes = {
    data :{
        destination_addresses : [ "233 State St, Brooklyn, NY 11201, USA" ],
        origin_addresses : [ "564 Hegeman Ave, Brooklyn, NY 11207, USA" ],
        rows : [
        {
            elements : [
                {
                    distance : {
                    text : "6.5 mi",
                    value : distance
                    },
                    duration : {
                    text : "33 mins",
                    value : 1987
                    },
                    status : "OK"
                }
            ]
        }],
        status : "OK"
    }
}

/**
 * Set up for each test file
 */
const setup = async()=>{
    axios.get.mockImplementation((url,options) => Promise.resolve(googleRes));
    await Order.deleteMany()
}


/**
 * Clear up data and close database after testing
 */
const down = async()=>{
    await Order.deleteMany()
    mongoose.disconnect()
}


module.exports={
    request,
    app,
    Order,
    HTTP,
    mongoose,
    axios,
    distance,
    setup,
    down
}