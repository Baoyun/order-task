/**
 * This file contains tests for Order module 
 */
const {Order,axios,mongoose,setup,down} = require('./setting')

var orderOneID = new mongoose.Types.ObjectId();
var orderOneData = {
    _id : orderOneID,
    origin: ["40.66","-73.89"],
    destination :["40.69","-73.99"]
}

var orderTwoID = new mongoose.Types.ObjectId();
var orderTwoData = {
    _id : orderTwoID,
    origin: ["40.66","-73.89"],
    destination :["40.69","-73.99"],
    status : true
}

beforeEach(async ()=>{
    await setup();
})

test('Distance calculation during creating Order',async ()=>{
   
    await new Order(orderOneData).save();
    const order = Order.findById(orderOneID);
    expect(order).not.toBeNull();
    orderOject = (await order).toObject();
    orderOneData.distance = 200;
    expect(orderOject).toMatchObject(orderOneData)

})


test('Order to JSON only return request data and in correct form(UNASSIGNED)',async ()=>{
   
    await new Order(orderOneData).save();
    const order = Order.findById(orderOneID);
    expect(order).not.toBeNull();
    orderOject = (await order).toJSON();
    expectedJson = { distance: 200, status: 'UNASSIGNED', id: orderOneID }
    expect(orderOject).toMatchObject(expectedJson)
    
})

test('Order to JSON only return request data and in correct form(ASSIGNED)',async ()=>{
   
    await new Order(orderTwoData).save();
    const order = Order.findById(orderTwoID);
    expect(order).not.toBeNull();
    orderOject = (await order).toJSON();
    expectedJson = { distance: 200, status: 'ASSIGNED', id: orderTwoID }
    expect(orderOject).toMatchObject(expectedJson)
    
})

test('Error in distance calculation during creating Order',async ()=>{

    axios.get.mockImplementation((url,options) => Promise.reject(new Error("error")));
    expect(new Order(orderOneData).save()).rejects.toThrow('Error in getting distance');

})

afterAll(async ()=>{
    await down()
})