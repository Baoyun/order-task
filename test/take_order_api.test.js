
/**
 * This file contains tests for take order API
 * PATCH /orders/:id
 */
const {request,app,Order,HTTP,mongoose,setup,down} = require('./setting')

const AsyncLock = require('async-lock');

var orderOneID = new mongoose.Types.ObjectId();
var orderTwoID = new mongoose.Types.ObjectId();
var orderThreeID = new mongoose.Types.ObjectId();

var orderOneData = {
    _id : orderOneID,
    origin: ["40.66","-73.89"],
    destination :["40.69","-73.99"]
}

var orderTwoData = {
    _id : orderTwoID,
    origin: ["40.66","-73.89"],
    destination :["40.69","-73.99"],
    status : true
}

var orderThreeData = {
    _id : orderThreeID,
    origin: ["40.66","-73.89"],
    destination :["40.69","-73.99"]
}

beforeEach(async ()=>{
    await setup();
    await new Order(orderOneData).save();
    await new Order(orderTwoData).save();
    await new Order(orderThreeData).save();
    
})

test('Take Orders Failed: Order does not exist', async () => {
    let id = new mongoose.Types.ObjectId();
    const res = await request(app).patch(`/orders/${id}`).send({
        status: "TAKEN"
    }).expect(HTTP.NOT_FOUND)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/does not exist/)
    })
})

test('Take Orders Failed: Passed in invalid data', async () => {
    const res = await request(app).patch(`/orders/${orderOneID}`).send({
        status: "NON-TAKEN"
    }).expect(HTTP.BAD_REQUEST)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/not valid/)
    })
})


test('Take Orders Success', async () => {
    const res = await request(app).patch(`/orders/${orderOneID}`).send({
        status: "TAKEN"
    }).expect(HTTP.SUCCESS)

    expect(res.body).toMatchObject({
        status: "SUCCESS"
    })

    const order = await Order.findById(orderOneID);

    expect(order.status).toBe(true)

})

test('Take Orders Failed: Order has already been taken', async () => {
    const res = await request(app).patch(`/orders/${orderTwoID}`).send({
        status: "TAKEN"
    }).expect(HTTP.FORBIDDEN)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/has been taken./)
    })
})


test('Concurrent Take Orders', async () => {
    const res1 = await request(app).patch(`/orders/${orderThreeID}`).send({
        status: "TAKEN"
    }).expect(HTTP.SUCCESS)

    const res2 = await request(app).patch(`/orders/${orderThreeID}`).send({
        status: "TAKEN"
    }).expect(HTTP.FORBIDDEN)

    expect(res1.body).toMatchObject({
        status: "SUCCESS"
    })

    const order = await Order.findById(orderThreeData);

    expect(order.status).toBe(true)

    expect(res2.body).toMatchObject({
        error: expect.stringMatching(/has been taken./)
    })
})

// This test must be at the end because we mock the find function
test('Create Order Fail : Lock error', async () => {
    const lockMock = jest.spyOn(AsyncLock.prototype, 'acquire');
    const lockAcquire = jest.fn((key, fn, cb, opts) => {
      //  throw new Error("Some error");
       return Promise.reject(new Error("error"));
    });
    lockMock.mockImplementation(lockAcquire);

    const res = await request(app).patch(`/orders/${orderThreeID}`).send({
        status: "TAKEN"
    }).expect(HTTP.SERVER_ERROR)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/Error in taking order/),
    })
    lockMock.mockRestore();
})


test('Create Order Fail : Server error', async () => {
    const OrderFindByIdMock = jest.spyOn(Order, 'findById');
    const OrderFindById = jest.fn(() => {
        throw new Error("Server error");
    });
    OrderFindByIdMock.mockImplementation(OrderFindById);

    const res = await request(app).patch(`/orders/${orderThreeID}`).send({
        status: "TAKEN"
    }).expect(HTTP.SERVER_ERROR)

    expect(res.body).toMatchObject({
        error: expect.stringMatching(/Server error in taking order/),
    })
    OrderFindByIdMock.mockRestore();
})

afterAll(async ()=>{
    await down()
})