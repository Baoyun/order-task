/**
 * This File contains the database connection
 */

const mongoose = require('mongoose')

const databaseUrl = process.env.DATABASE_URL
const databaseName = process.env.DATABASE_NAME

mongoose.connect(databaseUrl+databaseName,{
    useNewUrlParser:true,
    useCreateIndex:true,
    useUnifiedTopology: true 
})
