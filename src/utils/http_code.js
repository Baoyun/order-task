/**
 *  This file contains http code 
 */

const SUCCESS = 200;
const CREATED = 200;
const BAD_REQUEST = 400;
const FORBIDDEN = 403;
const NOT_FOUND = 404;
const SERVER_ERROR = 500;

module.exports = {
    SUCCESS,
    CREATED,
    BAD_REQUEST,
    FORBIDDEN,
    NOT_FOUND,
    SERVER_ERROR
}