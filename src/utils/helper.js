/**
 * This file contains helper functions
 */

const validator = require('validator')


/**
 * Validation function for page/limit
 * value must be at least one
 * @param {string} value 
 */
const intValidate = (value)=>{
    return validator.isInt(value,{min:1}) 
}


/**
 * Validation function for Coordinates
 * @param {any} value 
 * 
 */
const coordinatesValidate = (value)=>{

    if(!Array.isArray(value)){
        return {validate:false,errorMsg:"Coordinates must be an array."}
    }

    if(value.length != 2){
        return {validate:false,errorMsg:"Coordinates array must be 2 elements."}
    }

    for(element of value){
        if(typeof element !=='string'){
            return {validate:false,errorMsg:"Coordinates element must be string."}
       }
    }

    let coordinatesString = value.join(',');
    if(!validator.isLatLong(coordinatesString)){
       return {validate:false,errorMsg:"Coordinates are not valid."}
    }
    return {validate:true};
}


/**
 * Calclate and return skip number
 * @param {int} limit 
 * @param {int} page 
 */
const getSkip = (limit,page)=>{
    return (page-1)*limit;
}




/**
 * Function to create json contain error message passed in
 * @param {string} errMsg 
 */
const getErrorReturn = (errMsg)=>{
    return {
        error:errMsg
    }
}

module.exports={
    coordinatesValidate,
    intValidate,
    getSkip,
    getErrorReturn

}