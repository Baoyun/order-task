const axios = require('axios')

/**
 * Function to get distance from Google Map API
 * @param {string} origins  lat,long
 * @param {string} destinations  lat,long
 * @param callback 
 */
const getDistance = async (origins,destinations,callback) =>{
    try{
        const googleResponse = await axios.get(process.env.GOOGLE_API_URL,{
            params:{
                origins,
                destinations,
                key:process.env.GOOGLE_API_KEY
            }
          });
        let distance = googleResponse.data.rows[0].elements[0].distance.value; 
        callback(null,distance)
          
    }catch(e){
        callback("Error in getting distance: "+ e.code )
    }
    
}

module.exports = {
    getDistance
}
