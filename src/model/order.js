const mongoose = require('mongoose')
const distanceHelper = require('../utils/distance')

/**
 * Order module schema
 */
const orderSchema = mongoose.Schema({
    origin:{
        type:Array,
        required: true
    },
    destination:{
        type: Array,
        required: true
    },
    distance:{
        type: Number,
        default:0
    },
    status:{
        type:Boolean,
        default:0
    }
})

/**
 *  Calculate discount before save the order object
 */
orderSchema.pre('save',  async function(next){
    const order = this;
    if((order.isModified("origin"))||(order.isModified("destination"))){
        let origin = order.origin.join(',');
        let destination = order.destination.join(',');
        await distanceHelper.getDistance(origin,destination, (error,distance=0)=>{
            if(!error){
                order.distance = distance
                next();
            
            }else{
                next(new Error(error))
            }
        }) 
    }
})

/**
 *  Hide other data except the ones we want to return
 *  Convert status from boolean to string
 */
orderSchema.methods.toJSON=function(){
    const order = this;
    const orderObject = order.toObject({
        versionKey : false
    });
    
    const keepKey = ["id","distance","status"]
    let objectKey = Object.keys(orderObject);

    orderObject.id = orderObject._id;

    objectKey.forEach(key => {
        if(!keepKey.includes(key)){
            delete orderObject[key]
        }
    });

    if(orderObject.status){
        orderObject.status = 'ASSIGNED'
    }else{
        orderObject.status ='UNASSIGNED'
    }
    
    return orderObject;
}

const Order = mongoose.model('Order',orderSchema)

module.exports=Order;