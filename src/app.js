const express = require('express')
require('./database/mongoose')
const OrderRouter = require('./routers/order')

const app = express()

app.use(express.json())
app.use(OrderRouter)


module.exports=app;