const express = require('express')
const router = new express.Router();
const helper = require('../utils/helper')
const HTTP = require ('../utils/http_code')
const Order = require('../model/order')
const AsyncLock = require('async-lock');
const lock = new AsyncLock();


/**
 *    Ger orders
 *   /orders?page=:page&limit=:limit
 */
router.get('/orders',async (req,res)=>{

    var options = {};

    if(req.query.limit){
        if(!helper.intValidate(req.query.limit)){
            return res.status(HTTP.BAD_REQUEST).send(
                    helper.getErrorReturn("Limit is not an integer >= 1.")
                );
        }
        var limit = parseInt(req.query.limit);
        options.limit = limit;
    }
    
    if(req.query.page){
        if(limit === undefined){
            return res.status(HTTP.BAD_REQUEST).send(
                helper.getErrorReturn("Page must be passed in with limit")
            );
        }
        if(!helper.intValidate(req.query.page)){
            return res.status(HTTP.BAD_REQUEST).send(
                    helper.getErrorReturn("Page is not an integer >= 1.")
                );
        }
        var page = parseInt(req.query.page);
        options.skip = helper.getSkip(limit,page);
    }

    try{
        const orders = await Order.find(null,null,options);
        res.status(HTTP.SUCCESS).send(orders);
    }catch{
        res.status(HTTP.SERVER_ERROR).send(
            helper.getErrorReturn("Server error in getting orders")
        );
    }

})

/**
 *  Create Order
 */
router.post('/orders',async (req,res)=>{
    const data = Object.keys(req.body);
    const requireKey = ['origin','destination'];

    const isValid = data.every((key)=>requireKey.includes(key));
    if(!isValid){
        return res.status(HTTP.BAD_REQUEST).send(
            helper.getErrorReturn("Data is not allowed")
        );
        
    }
    
    const isDataComplete = requireKey.every((key)=>data.includes(key));
    if(!isDataComplete){
        return res.status(HTTP.BAD_REQUEST).send(
            helper.getErrorReturn("Missing Data")
        );
        
    }

    for(key of requireKey){
        let validationResult = helper.coordinatesValidate(req.body[key])
        if(!validationResult.validate){
            let errorMsg = `${key} is not valid: ${validationResult.errorMsg}`;
            return res.status(HTTP.BAD_REQUEST).send(
                helper.getErrorReturn(errorMsg)
            );
        }
    }

    const order = new Order (req.body);
    try{
        await order.save();
        res.status(HTTP.CREATED).send(order);
    }catch(e){
        res.status(HTTP.SERVER_ERROR).send(
            helper.getErrorReturn(`Server error in saving order: ${e}`)
        );
    }
   
})

/**
 *  Take Order
 */
router.patch('/orders/:id',async(req,res)=>{
    
    const _id = req.params.id;
    const processKey = `take ${_id}`;

    const status = req.body.status;
    if(status!=='TAKEN'){
        return res.status(HTTP.BAD_REQUEST).send(
            helper.getErrorReturn("Data does not valid.")
        );
    }

    // lock the order in case other requests come in at the same time
    lock.acquire(processKey,async ()=>{
        try{
            const order = await Order.findById(_id);
            if(!order){
                return res.status(HTTP.NOT_FOUND).send(
                    helper.getErrorReturn(`Order with id ${_id} does not exist`)
                );
            }
            if(order.status){
                return res.status(HTTP.FORBIDDEN).send(
                    helper.getErrorReturn(`Order with id ${_id} has been taken.`)
                );
            }
            order.status = true;
            await order.save();
            res.status(HTTP.SUCCESS).send({
                status: "SUCCESS"
            });
            
        } catch(e){
            return res.status(HTTP.SERVER_ERROR).send(
                helper.getErrorReturn(`Server error in taking order with id: ${_id}`)
            );
        }   
    }).catch((e)=>{
        return res.status(HTTP.SERVER_ERROR).send(
            helper.getErrorReturn(`Error in taking order with id  + ${_id}`)
        );
    }) 
    
})

module.exports=router;